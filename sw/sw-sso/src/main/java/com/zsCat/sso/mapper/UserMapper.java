package com.zsCat.sso.mapper;

import com.github.abel533.mapper.Mapper;
import com.zsCat.sso.pojo.User;

public interface UserMapper extends Mapper<User>{

}
